module.exports = {
    purge: [
        './_includes/**/*.html',
        './_layouts/**/*.html',
        './*.html',
    ],
    darkMode: false,
    theme: {
        extend: {
            colors: {
                'steel': {
                    '50': '#5f7083',
                    '100': '#556679',
                    '200': '#4b5c6f',
                    '300': '#415265',
                    '400': '#37485b',
                    '500': '#2d3e51',
                    '600': '#233447',
                    '700': '#192a3d',
                    '800': '#0f2033',
                    '900': '#051629'
                },
                'supernova': {
                    '50': '#ff7059',
                    '100': '#ff664f',
                    '200': '#fe5c45',
                    '300': '#f4523b',
                    '400': '#ea4831',
                    '500': '#e03e27',
                    '600': '#d6341d',
                    '700': '#cc2a13',
                    '800': '#c22009',
                    '900': '#b81600'
                }
            }
        },
    },
    variants: {
        extend: {
            borderWidth: ['first', 'last']
        }
    },
    plugins: [
        require('@tailwindcss/forms')
    ]
}