---
image: /assets/images/engagement.jpg
title: Engagement
description: Through partnership and empathetic listening we are able to get you a plan, consumption guide, and exhaustive reportage of what energy setup best serves your needs.
order: 1
list:
    - Energy Planning
    - Managed Consumption
    - Reporting
    - Advisory & Insights
link: /approach/
---
Our work starts with your energy needs and goals. Engaging you on those needs through partnership and empathetic listening we are able to fully understand them. We then see how what you are aiming for matches an energy mix that is both technically and economically feasible for you.

At the end, you might get a plan, a consumption documentation and guide, and/or an exhaustive reportage of what energy setup would serve your needs efficiently.