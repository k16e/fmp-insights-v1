---
image: /assets/images/strategy.jpg
title: Strategy & Evaluation
description: You get a complete toolkit of your efficient and resilient energy solutions, replete with our thinking behind the recommendations, best-value tactics, implementation plans, giving you full control over your consumption.
order: 2
list:
    - Energy Roadmapping
    - Research & Development
    - Resource Sourcing
    - Solution Architecting
link: /approach/
---
Our strategy gives you a complete toolkit of your efficient and resilient energy solutions.

Your toolkit is replete with our thinking behind the recommendations, best-value tactics, implementation plans, resource needs, including sourcing and deployment.

Our strategy puts you in full control of your energy consumption.