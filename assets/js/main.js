(function () {
    'use strict';

    let
        drawer = q('.side-drawer'),
        trigger = q('.nav-toggle')


    // Picking colors from https://purple11.com/complementary-colors/
    let colors = {
        supernova: '#e03e27',
        greentone: '#00a933',
        yelloworange: '#ffae42',
        violetred: '#c71585',
        orangeyellow: '#f2bb05',
        majorelleblue: '#623cea',
        platinum: '#e5e7eb',
        fireopal: '#ef6461',
        verdigris: '#2ca6a4',
        melon: '#f5bfb8'
    }


    // Materializecss Inits
    let
        dropdownInstances = M.Dropdown.init(nl('.dropdown-trigger'), { alignment: 'right', coverTrigger: false }),
        collapsibleInstances = M.Collapsible.init(nl('.collapsible'), {}),
        sidenavInstances = M.Sidenav.init(nl('.sidenav'), { edge: 'right', draggable: false }),
        selectInstances = M.FormSelect.init(nl('select'), {}),
        modalInstances = M.Modal.init(nl('.modal'), {}),
        tabsInstances = M.Tabs.init(nl('.tabs'), {}),
        instance = M.Collapsible.getInstance(q('.nav-list'))

    // Sidebar Nav Actions
    drawer.addEventListener('mouseenter', () => {
        body.classList.add('overlay-nav')
    })
    drawer.addEventListener('mouseleave', () => {
        instance.close()
        body.classList.remove('overlay-nav')
    })
    trigger.addEventListener('click', () => body.classList.toggle('nav-is-visible'), false)


    // Map Infobox Toggling
    if (q('.r-infobox')) {
        let
            infoboxOpener = q('.r-infobox__open'),
            infoboxCloser = q('.r-infobox__close'),
            infoboxContent = q('.r-infobox__content')

        infoboxCloser.addEventListener('click', () => {
            if (infoboxContent.classList.contains('scale-100')) {
                infoboxContent.classList.remove('scale-100')
                infoboxContent.classList.add('scale-0')
                infoboxOpener.classList.remove('scale-0')
                infoboxOpener.classList.add('scale-100')
            }
        }, false)
        infoboxOpener.addEventListener('click', () => {
            if (infoboxContent.classList.contains('scale-0')) {
                infoboxContent.classList.remove('scale-0')
                infoboxContent.classList.add('scale-100')
                infoboxOpener.classList.add('scale-0')
                infoboxOpener.classList.remove('scale-100')
            }
        }, false)
    }


    // Chartjs
    if (q('#bar-chart')) {
        let ctx = q('#bar-chart').getContext('2d')
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Osogbo', 'Agbor', 'Sango', 'Potiskum', 'Egbeda', 'Sapele', 'Kaani'],
                datasets: [
                    {
                        label: 'Growth Size',
                        backgroundColor: ['#e03e27'],
                        data: [2478, 5267, 734, 11784, 433, 7894, 9001],
                        borderRadius: 8,
                    }
                ]
            },
            options: {
                responsive: false,
                plugins: {
                    legend: { position: 'bottom', align: 'start' },
                    title: { display: false }
                }
            }
        })
    }

    if (q('#doughnut-chart')) {
        let ctx = q('#doughnut-chart').getContext('2d')
        new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Osogbo', 'Agbor', 'Sango', 'Potiskum', 'Egbeda'],
                datasets: [
                    {
                        label: '...',
                        backgroundColor: [
                            colors.supernova, colors.greentone, colors.yelloworange, colors.violetred, colors.orangeyellow
                        ],
                        data: [5267, 2478, 734, 784, 433]
                    }
                ]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom', align: 'start' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#visit-doughnut')) {
        let ctx = q('#visit-doughnut').getContext('2d')
        new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Productive Visits', 'Non-Productive Visits'],
                datasets: [
                    {
                        label: '...',
                        backgroundColor: [colors.supernova, colors.platinum],
                        data: [5267, 2478]
                    }
                ]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom', align: 'start' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#pie-1')) {
        let ctx = q('#pie-1').getContext('2d')
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Sales', ''],
                datasets: [{
                    label: '...',
                    backgroundColor: [colors.fireopal, colors.platinum],
                    data: [72, 28]
                }]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#pie-2')) {
        let ctx = q('#pie-2').getContext('2d')
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Visits', ''],
                datasets: [{
                    label: '...',
                    backgroundColor: [colors.majorelleblue, colors.platinum],
                    data: [55, 45]
                }]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#pie-3')) {
        let ctx = q('#pie-3').getContext('2d')
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Productivity', ''],
                datasets: [{
                    label: '...',
                    backgroundColor: [colors.verdigris, colors.platinum],
                    data: [24, 76]
                }]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#line-chart')) {
        let ctx = q('#line-chart').getContext('2d')
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
                datasets: [
                    {
                        data: [5, 2, 8, 3],
                        label: 'Africa',
                        borderColor: colors.supernova,
                        fill: false,
                        tension: 0.4
                    }
                ]
            },
            options: {
                responsive: false,
                plugins: {
                    legend: { display: false },
                    title: { display: false }
                }
            }
        })
    }

    if (q('#bar-chart-grouped')) {
        let ctx = q('#bar-chart-grouped').getContext('2d')
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['1900', '1950', '1999', '2050'],
                datasets: [
                    {
                        label: 'Projected',
                        backgroundColor: colors.melon,
                        data: [133, 221, 783, 2478],
                        borderRadius: 8
                    },
                    {
                        label: 'Actual',
                        backgroundColor: colors.supernova,
                        data: [408, 547, 675, 734],
                        borderRadius: 8
                    }
                ]
            },
            options: {
                responsive: false,
                plugins: {
                    legend: { position: 'bottom', align: 'start' },
                    title: { display: false }
                }
            }
        })
    }

    if (q('#pie-4')) {
        let ctx = q('#pie-4').getContext('2d')
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Retail', 'Wholesale'],
                datasets: [{
                    label: 'Wholesale',
                    backgroundColor: [colors.supernova, colors.greentone],
                    data: [72, 28]
                }]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#pie-5')) {
        let ctx = q('#pie-5').getContext('2d')
        new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Retailers'],
                datasets: [{
                    label: 'Not Sure Of This',
                    backgroundColor: [colors.orangeyellow],
                    data: [100]
                }]
            },
            options: {
                plugins: {
                    legend: { position: 'bottom' },
                    title: { display: false }
                },
                responsive: false
            }
        })
    }

    if (q('#line-2')) {
        let ctx = q('#line-2').getContext('2d')
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
                datasets: [
                    {
                        data: [5, 2, 8, 3],
                        label: 'Africa',
                        borderColor: colors.supernova,
                        fill: false,
                        tension: 0.4
                    }
                ]
            },
            options: {
                responsive: false,
                plugins: {
                    legend: { display: false },
                    title: { display: false }
                }
            }
        })
    }


    // Outlet Contacts Data Table
    let outletContactData = {
        'headings': [
            'Name',
            'Company',
            'Ext.',
            'Start Date',
            'Email',
            'Phone No.'
        ],
        'data': [
            [
                'Hedwig F. Nguyen',
                'Arcu Vel Foundation',
                '9875',
                '03/27/2017',
                'nunc.ullamcorper@metusvitae.com',
                '070 8206 9605'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Hedwig F. Nguyen',
                'Arcu Vel Foundation',
                '9875',
                '03/27/2017',
                'nunc.ullamcorper@metusvitae.com',
                '070 8206 9605'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Hedwig F. Nguyen',
                'Arcu Vel Foundation',
                '9875',
                '03/27/2017',
                'nunc.ullamcorper@metusvitae.com',
                '070 8206 9605'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Hedwig F. Nguyen',
                'Arcu Vel Foundation',
                '9875',
                '03/27/2017',
                'nunc.ullamcorper@metusvitae.com',
                '070 8206 9605'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Hedwig F. Nguyen',
                'Arcu Vel Foundation',
                '9875',
                '03/27/2017',
                'nunc.ullamcorper@metusvitae.com',
                '070 8206 9605'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                'Nullam.vitae@egestas.edu',
                '0800 106980'
            ],
            [
                'Genevieve U. Watts',
                'Eget Incorporated',
                '9557',
                '07/18/2017',
                '<a href="">Nullam.vitae@egestas.edu</a>',
                '0800 106980'
            ]
        ]
	}
    let outletContactDataTable = new simpleDatatables.DataTable('#outlet-contacts-datatable', {
        fixedHeight: true,
        perPage: 5,
        labels: {
            perPage: '{select} <span>Entries</span>',
        }
    })
    outletContactDataTable.insert(outletContactData)
})()